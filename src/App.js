import "./App.css";
import Border from "./components/Border";
import Cor from "./components/Cor/Cor";
import Font from "./components/FontWeight/Font";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Border title="Primeiro teste">
          <Cor color="blue">Primeiro componente</Cor>
        </Border>
        <Border title="Segundo teste">
          <Font fontWeight="bolder">Primeiro componente</Font>
        </Border>
      </header>
    </div>
  );
}

export default App;
