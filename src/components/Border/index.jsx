import { Component } from "react";
import "./index.css";

class Border extends Component {
  render() {
    return (
      <div className="border">
        <h1>{this.props.title} </h1>
        <div>{this.props.children}</div>
      </div>
    );
  }
}
export default Border;
