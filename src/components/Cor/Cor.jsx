import { Component } from "react";

class Cor extends Component {
  render() {
    return (
      <>
        <div style={{ color: this.props.color }}>{this.props.children}</div>
      </>
    );
  }
}
export default Cor;
