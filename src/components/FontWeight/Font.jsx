import { Component } from "react";

class Font extends Component {
  render() {
    return (
      <>
        <div style={{ fontWeight: this.props.fontWeight }}>
          {this.props.children}
        </div>
      </>
    );
  }
}
export default Font;
